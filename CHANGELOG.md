# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/a9065193978/gradle-example/compare/v0.0.2...v0.0.3) (2022-03-29)


### Features

* add git-hooks ([15577ad](https://gitlab.com/a9065193978/gradle-example/commit/15577ada7d4294769b18aa10ad1159b78e1a29a2))

### [0.0.2](https://gitlab.com/a9065193978/gradle-example/compare/v0.0.1...v0.0.2) (2022-03-29)


### Features

* delete config x ([2695482](https://gitlab.com/a9065193978/gradle-example/commit/2695482a324c13fabf3b161a4c6d4d63327a64a9))

### 0.0.1 (2022-03-29)


### Features

* change build gradle ([63e0086](https://gitlab.com/a9065193978/gradle-example/commit/63e0086c80c164d99f2241f79608f1f3ac788bf0))
* up ([f89c03a](https://gitlab.com/a9065193978/gradle-example/commit/f89c03af10af6eaab3b472da59a5e8b3b7dab633))
* version ([fc39f1e](https://gitlab.com/a9065193978/gradle-example/commit/fc39f1e841ac090b960ad2a0e3f22aefc359ba47))


### Bug Fixes

* cc ([cd8e5f3](https://gitlab.com/a9065193978/gradle-example/commit/cd8e5f350211110d9e23084a7b73681624d65af2))
* change ([cd22a5d](https://gitlab.com/a9065193978/gradle-example/commit/cd22a5de6d5db6a74d34a1acaf69d94135f8ecf6))
* test ([59d6213](https://gitlab.com/a9065193978/gradle-example/commit/59d6213db62f671b21eb5ca98d4054b2dcfe5038))
* up ([95f2031](https://gitlab.com/a9065193978/gradle-example/commit/95f2031a7acbf9ba2b9809f61f7965dd037dcd40))
* up ([e6ff162](https://gitlab.com/a9065193978/gradle-example/commit/e6ff162e41bca7c2cf2d6a06fdb85ddeea1904c1))
