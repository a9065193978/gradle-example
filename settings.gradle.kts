rootProject.name = "esi"

// Core
include("core:application")
include("core:database")

// Shared
include("common")

// Integrators
