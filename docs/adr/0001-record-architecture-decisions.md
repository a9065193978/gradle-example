# Запись архитектурных решений

* Статус: принято
* Принявшие решение: Aleksey Tsvetkov, Ivan Emelyanov, Andrey Pogulyaev
* Дата: 2022-03-15

## Контекст и описание проблемы

Нам нужно записать архитектурные решения, принятые в этом проекте.

## Факторы принятия решения <!-- опционально -->

* Стандартизировать и записывать все решения

## Рассмотренные варианты

* [Architecture decision record (ADR)](https://github.com/joelparkerhenderson/architecture-decision-record)

## Итоговое решение

Выбран вариант: "ADR", потому что на данный момент это единственно найденный вариант.

## Последствия <!-- опционально -->

* Улучшение качества и скорости разработки
* Уменьшение времени, затраченного на ревью
* Избавление от споров по реализации того или иного функционала

## Ссылки <!-- опционально -->

* [Github](https://github.com/joelparkerhenderson/architecture-decision-record)
* [DOCUMENTING ARCHITECTURE DECISIONS](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions)
