# [короткий заголовок проблемы и решения]

* Статус: [предложение | отвергнуто | принято | устарело]
* Принявшие решение: [список всех кто был вовлечен в принятие решения]
* Дата: [YYYY-MM-DD когда решение было обновлено]

Техническая история: [описание | ссылка на запрос/задачу ] <!-- опционально -->

## Контекст и описание проблемы

[Описание контекста и проблемы, например, в простой форме, двумя или тремя предложениями. Вы можете выразить проблему в форме вопроса.]

## Факторы принятия решения <!-- опционально -->

* [ограничение 1, например, фактор, проблема с которой столкнулись, …]
* [ограничение 2, например, фактор, проблема с которой столкнулись, …]
* … <!-- количество факторов при принятии решения может быть разным -->

## Рассмотренные варианты

* [вариант 1]
* [вариант 2]
* [вариант 3]
* … <!-- количество вариантов может быть разным -->

## Итоговое решение

Выбран вариант: "[вариант 1]", потому что [обоснование. например, только один вариант, который удовлетворяет ограничениям | который решает проблему | … | подходит лучше всего].

## Последствия <!-- опционально -->

* [например., улучшение качественных показателей, следует принять решения, …]
* …

## Ссылки <!-- опционально -->

* [Вид ссылки] [Ссылка на ADR] <!-- например: Предложение [ADR-0005](0005-example.md) -->
* [Вид ссылки] [Ссылка на внешний источник] <!-- например: Предложение [ADR-0005](0005-example.md) -->
* … <!-- количество ссылок может быть разным -->
