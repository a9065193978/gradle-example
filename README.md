# ESI

ESI - external services integration (integration of external services) - интеграция внешних сервисов. Проект должен работать со сложными интеграциями (парсинг сайтов, нестабильные api и т.д.).

## Requirements
- Use [sdkman](http://sdkman.io/)

## Установка
1. установить git hooks
```sh
./contrib/git/install-hooks
```

## Spring Banner Generator
https://springhow.com/spring-boot-banner-generator, тип `ANSI Shadow`

## Документация
Документация по проекту расположена в каталоге [docs](./docs/README.md)

## Смена версии java
1. Меняем версию в [.sdkmanrc](./.sdkmanrc)
2. Меняем версию в [.gitlab-ci.yml](./.gitlab-ci.yml), переменная `JAVA_VERSION`
