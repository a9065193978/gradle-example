plugins {
    id("ru.p_credit.esi.application-conventions")
}

dependencies {
    implementation(group = Groups.spring, name = "spring-boot-starter-web")
    implementation(group = Groups.jackson, name = "jackson-module-kotlin")
    implementation(
        group = Groups.akkinoc,
        name = "logback-access-spring-boot-starter",
        version = Versions.akkinoc
    )
    implementation(group = Groups.spring, name = "spring-boot-starter-actuator")
    implementation(
        group = Groups.springdoc,
        name = "springdoc-openapi-ui",
        version = Versions.springdoc
    )
    implementation(
        group = Groups.springdoc,
        name = "springdoc-openapi-kotlin",
        version = Versions.springdoc
    )
}

springBoot {
    buildInfo {
        properties {
            name = "Integration of external services"
        }
    }
}
