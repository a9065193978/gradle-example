package ru.p_credit.esi.core

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CoreApplication

fun main(args: Array<String>) {
    @Suppress("SpreadOperator")
    runApplication<CoreApplication>(*args)
}
