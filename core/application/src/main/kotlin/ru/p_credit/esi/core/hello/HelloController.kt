package ru.p_credit.esi.core.hello

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/hello")
@Tag(name = "hello", description = "hello API")
class HelloController {
    @GetMapping()
    fun get(@RequestParam(defaultValue = "unknown") name: String) = "Hello, $name!"
}
