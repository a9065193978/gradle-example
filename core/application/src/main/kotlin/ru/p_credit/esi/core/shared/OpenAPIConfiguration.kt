package ru.p_credit.esi.core.shared

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenAPIConfiguration {
    @Bean
    fun openApi(build: BuildProperties) = OpenAPI().apply {
        info = Info().apply {
            title = build.name
            version = build.version
        }
    }
}
