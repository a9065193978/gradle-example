plugins {
    id("ru.p_credit.esi.library-conventions")
    id("nu.studer.jooq")
}

dependencies {
    api(group = Groups.jooq, name = "jooq")
    api(group = Groups.jooq, name = "jooq-kotlin")

    implementation(group = Groups.jooq, name = "jooq-meta")
    implementation(group = Groups.jooq, name = "jooq-codegen")
}

jooq {
    version.set(Versions.jooq)
}
