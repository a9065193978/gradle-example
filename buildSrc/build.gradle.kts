val kotlinVersion: String by project
val detektVersion: String by project

plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

dependencies {
    implementation(kotlin(module = "gradle-plugin", version = kotlinVersion))
    implementation(kotlin(module = "allopen", version = kotlinVersion))

    implementation(
        group = "io.gitlab.arturbosch.detekt",
        name = "detekt-gradle-plugin",
        version = detektVersion
    )
    implementation(
        group = "org.springframework.boot",
        name = "spring-boot-gradle-plugin",
        version = "2.6.4"
    )
    implementation(
        group = "io.spring.gradle",
        name = "dependency-management-plugin",
        version = "1.0.11.RELEASE"
    )
    implementation(group = "nu.studer", name = "gradle-jooq-plugin", version = "6.0.1")
}

/*
gradlePlugin {
    plugins {
        create("semanticBuildVersioningPlugin") {
            id = "net.vivin.gradle-semantic-build-versioning"
            implementationClass = "net.vivin.gradle.versioning.SemanticBuildVersioningPlugin"
        }
    }
}*/

/*
pluginBundle {
    website = "https://gitlab.com/a9065193978/gradle-example"
    vcsUrl = "https://gitlab.com/a9065193978/gradle-example"
    description = "This is a Gradle settings-plugin that provides support for semantic versioning of builds. It is quite easy to use and extremely configurable. The plugin allows you to bump the major, minor, patch or pre-release version based on the latest version, which is identified from a git tag. It also allows you to bump pre-release versions based on a scheme that you define. The version can be bumped by using version-component-specific project properties or can be bumped automatically based on the contents of a commit message. If no manual bumping is done via commit message or project property, the plugin will increment the version-component with the lowest precedence; this is usually the patch version, but can be the pre-release version if the latest version is a pre-release one. The plugin does its best to ensure that you do not accidentally violate semver rules while generating your versions; in cases where this might happen the plugin forces you to be explicit about violating these rules. As this is a settings plugin, it is applied to settings.gradle and version calculation is therefore performed right at the start of the build, before any projects are configured. This means that the project version is immediately available (almost as if it were set explicitly - which it effectively is), and will never change during the build (barring some other, external task that attempts to modify the version during the build). While the build is running, tagging or changing the project properties will not influence the version that was calculated at the start of the build."
    tags = listOf("versioning", "semantic-versioning", "git", "build-versioning", "auto-versioning", "version")

    plugins {
        create("semanticBuildVersioningPlugin") {
            id = "net.vivin.gradle-semantic-build-versioning"
            displayName = "Gradle Semantic Build Versioning Plugin"
        }
    }
}
*/
