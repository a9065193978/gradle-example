import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.DetektCreateBaselineTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val version: String by project
val javaVersion = JavaVersion.VERSION_11

group = "ru.p_credit"

plugins {
    kotlin("jvm")

    id("io.gitlab.arturbosch.detekt")
}

java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    detektPlugins(group = Groups.detekt, name = "detekt-formatting", version = Versions.detekt)
}

detekt {
    toolVersion = Versions.detekt
    config = files("$rootDir/detekt.yml")
    buildUponDefaultConfig = true
    allRules = false
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = javaVersion.toString()
        }
    }

    withType<Jar> {
        val complexName = "${rootProject.name}-${project.name}"
        val vendor = System.getProperty("java.vendor")
        val createdBy = "${System.getProperty("java.version")} ($vendor)"

        archiveBaseName.set(complexName)
        manifest {
            attributes(
                "Created-By" to createdBy,
                "Implementation-Title" to complexName,
                "Implementation-Version" to archiveVersion,
                "Implementation-Vendor" to "Platform",
            )
        }
    }

    withType<Test> {
        useJUnitPlatform()
    }

    withType<Detekt>().configureEach {
        jvmTarget = javaVersion.toString()
    }

    withType<DetektCreateBaselineTask>().configureEach {
        jvmTarget = javaVersion.toString()
    }
}
