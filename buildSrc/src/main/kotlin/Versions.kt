object Versions {
    const val jooq = "3.15.8"
    const val akkinoc = "3.2.2"
    const val detekt = "1.19.0"
    const val springdoc = "1.6.6"
}
