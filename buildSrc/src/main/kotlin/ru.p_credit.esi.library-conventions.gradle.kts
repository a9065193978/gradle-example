import org.gradle.kotlin.dsl.`java-library`

plugins {
    id("ru.p_credit.esi.common-conventions")
    `java-library`
}
