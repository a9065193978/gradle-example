val artifactsDir = rootProject.layout.buildDirectory.dir("artifacts")

plugins {
    id("ru.p_credit.esi.common-conventions")

    id("org.springframework.boot")
    id("io.spring.dependency-management")

    kotlin("plugin.spring")
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

dependencies {
    implementation(group = Groups.spring, name = "spring-boot-starter")

    developmentOnly(group = Groups.spring, name = "spring-boot-devtools")

    annotationProcessor(group = Groups.spring, name = "spring-boot-configuration-processor")

    testImplementation(group = Groups.spring, name = "spring-boot-starter-test")
}

tasks {
    register<Delete>("cleanArtifacts") {
        delete(artifactsDir)
    }

    register<Copy>("artifacts") {
        description = """
            Задача для сбора jar файлов в одном месте. В основном нужно для артефактов Gitlab.
            Gitlab сохраняет в артефактах всю вложенность каталогов.
            Если мы копируем файлы от сюда core/application/build/libs/*.jar, то в архиве будет
            такой же путь. Данное задание собирает все jar файлы в корневом каталоге сборки
            `build/artifacts`. Эту задачу нужно запускать после `assemble`
        """.trimIndent()

        dependsOn("cleanArtifacts")

        from(layout.buildDirectory.dir("libs"))
        include("*.jar")
        into(artifactsDir)
    }
}
