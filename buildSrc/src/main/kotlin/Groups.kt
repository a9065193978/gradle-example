object Groups {
    const val project = "ru.p_credit.esi"
    const val spring = "org.springframework.boot"
    const val jackson = "com.fasterxml.jackson.module"
    const val jooq = "org.jooq"
    const val akkinoc = "dev.akkinoc.spring.boot"
    const val detekt = "io.gitlab.arturbosch.detekt"
    const val springdoc = "org.springdoc"
}
